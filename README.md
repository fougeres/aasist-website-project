# Aasist website project

The goal of this project is to allow to use an existing project 'Aasist project' on git, to transform it to use it on a local web page.
To do this I used the NodeJs technology, in order to execute python files from a website.
I then had to modify the existing python project, so that instead of evaluating all the documents, the user can choose one of the files and test it alone.
The website returns the score of the audio file, as well as its description if it is spoof or bonafide.

## Installation

To install, you must first install nodejs on your computer.
To do this in the main folder, you can run its installation through the file, and follow the differents steps :
[installation nodejs](node-v16.14.2-x64.msi)

After that, you need to install npm (default package manager for the Node.js JavaScript runtime environment).
Just click on the executable file:
[installation npm](installnpm.bat)

Please also install beforehand the file: 'requirements.txt', allowing to install the different python libraries, present in the initial project AASIST PROJECT.

## Launch of the website

To launch the compilation of the project, just click on the file:
[launching node](scriptwindows.bat)

Then go to your search browser, and copy this URL:
-   http://localhost:8080/

## Functioning of the website

First of all when you arrive on the web site, you will be able to drag and drop an audio file of this form 'LA_E_*********.flac'. If you do not have a file like that you can find it here: config/LA/ASVspoof2019_LA_evalO/flac/.

After that you just have to click on the button to start the analysis.

## Display of results

After starting the analysis, you have to wait about 20 seconds before you get to the results page.
On this page, a graph is displayed with two curves and the score of the analyzed file (in the form of points).

Just below this graph, a sentence is displayed to tell you that your file is bonafide or spoof, as well as its precise score.

## Download of results

The download is automatic to find your analyses, you can go to this folder: [repository result](public/results).
The recorded images are the graphs, and the names assigned to the analyses are the names of the audio files and their scores.

## Compilation error

If the website does not work, this is due to a lack of python libraries, to do this, I invite you to test if the initial project: [aasist project](https://github.com/clovaai/aasist).

If it still doesn't work, you can contact me by email: hugo.fougeres@eurecom.fr.
