import sys
from mutagen.wave import WAVE
from mutagen.mp3 import MP3

def audio_duration(length):
    hours = length // 3600  # calculate in hours
    length %= 3600
    mins = length // 60  # calculate in minutes
    length %= 60
    seconds = length  # calculate in seconds
  
    return hours, mins, seconds  # returns the duration
  
# Create a WAVE object
# Specify the directory address of your wavpack file
# "alarm.wav" is the name of the audiofile

#audio = WAVE(sys.argv[1])

#print(sys.argv[1])
audio = WAVE(sys.argv[1])
#print(int(audio.info.lenght))
#audio = sys.argv[1]

# contains all the metadata about the wavpack file
audio_info = audio.info
length = int(audio_info.length)
hours, mins, seconds = audio_duration(length)
print('Total Duration: {}:{}:{}'.format(hours, mins, seconds))

#f=open(str(sys.argv[1]))
#for ligne in f:
#    print(ligne.strip())


#print("Welcome to", str(sys.argv))