"""
Main script that trains, validates, and evaluates
various models including AASIST.

AASIST
Copyright (c) 2021-present NAVER Corp.
MIT license
"""
import argparse
from email.mime import audio
import json
import os
import sys
import warnings
from importlib import import_module
from pathlib import Path
from shutil import copy
from typing import Dict, List, Union

import asyncio
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from torchcontrib.optim import SWA

from data_utils import (Dataset_ASVspoof2019_train,
                        Dataset_ASVspoof2019_devNeval, genSpoof_list, Dataset_ASVspoof2019_devNeval_all)
from evaluation import calculate_tDCF_EER
from utils import create_optimizer, seed_worker, set_seed, str_to_bool

warnings.filterwarnings("ignore", category=FutureWarning)


def main(args: argparse.Namespace) -> None:
    """
    Main function.
    Trains, validates, and evaluates the ASVspoof detection model.
    """
    # load experiment configurations
    #args.config= "./config/AAsist.conf"
    with open("./config/AAsist.conf", "r") as f_json:
        config = json.loads(f_json.read())
    model_config = config["model_config"]
    optim_config = config["optim_config"]
    optim_config["epochs"] = config["num_epochs"]
    track = config["track"]
    assert track in ["LA", "PA", "DF"], "Invalid track given"
    if "eval_all_best" not in config:
        config["eval_all_best"] = "True"
    if "freq_aug" not in config:
        config["freq_aug"] = "False"

    # make experiment reproducible
    set_seed(args.seed, config)

    # define database related paths
    output_dir = Path(args.output_dir)
    prefix_2019 = "ASVspoof2019.{}".format(track)
    database_path = Path(config["database_path"])
    dev_trial_path = ("config/LA/ASVspoof2019_LA_cm_protocols/ASVspoof2019.LA.cm.dev.trl.txt")
    eval_trial_path = ("config/LA/ASVspoof2019_LA_cm_protocols/ASVspoof2019.LA.cm.evaltest.trl.txt")

    # define model related paths
    model_tag = "{}_{}_ep{}_bs{}".format(
        track,
        os.path.splitext(os.path.basename("./config/AAsist.conf"))[0],
        config["num_epochs"], config["batch_size"])
    
    model_tag = output_dir / model_tag
    model_save_path = model_tag / "weights"
    eval_score_path = model_tag / config["eval_output"]
    writer = SummaryWriter(model_tag)
    os.makedirs(model_save_path, exist_ok=True)
    copy("./config/AAsist.conf", model_tag / "config.conf")

    # set device
    device = "cpu"

    
    # evaluates pretrained model and exit script
    if args.eval:
        
        originalfile= open('./config/namefile.txt','r')
        originalname= originalfile.readlines()
        oritest='{}'.format(originalname)
        originalnameconcat= oritest[2:-2]
        audiofile = '.'.join(originalnameconcat.split('.')[:-1])
        asyncio.run(get_file_protocol(audiofile))
        print ("hello")
        sys.exit(0)

    # get optimizer and scheduler
    best_dev_eer = 1.
    best_eval_eer = 100.
    best_dev_tdcf = 0.05
    best_eval_tdcf = 1.
    n_swa_update = 0  # number of snapshots of model to use in SWA
    f_log = open(model_tag / "metric_log.txt", "a")
    f_log.write("=" * 5 + "\n")

    # make directory for metric logging
    metric_path = model_tag / "metrics"
    os.makedirs(metric_path, exist_ok=True)


async def get_file_protocol(audioname):
    namefile= open("config/LA/ASVspoof2019_LA_cm_protocols/ASVspoof2019.LA.cm.evaltest.trl.txt","w+")

    with open('config/LA/ASVspoof2019_LA_cm_protocols/ASVspoof2019.LA.cm.eval.trl.txt','r') as f:
        lines = f.read().split("\n")
    for i,line in enumerate(lines):
        if audioname in line:
            r=namefile.write(line)
    
    await asyncio.sleep(5)
    path="config/LA/ASVspoof2019_LA_cm_protocols/ASVspoof2019.LA.cm.evaltest.trl.txt"

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="ASVspoof detection system")
    parser.add_argument("--config",
                        dest="config",
                        type=str,
                        help="configuration file",
                        )
    parser.add_argument(
        "--eval",
        action="store_true",
        default="store_true",
        help="when this flag is given, evaluates given model and exit")
    parser.add_argument(
        "--output_dir",
        dest="output_dir",
        type=str,
        help="output directory for results",
        default="./exp_result",
    )
    parser.add_argument("--seed",
                        type=int,
                        default=1234,
                        help="random seed (default: 1234)")
    main(parser.parse_args())
