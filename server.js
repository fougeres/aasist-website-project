// load the things we need
var express = require('express');
var app = express();
var scriptOutput = "";
var scriptOutput1 = "";
var fs = require('fs');
var fileupload = require("express-fileupload");

const bodyParser = require('body-parser');
const {spawn} = require('child_process');

// set the view engine to ejs
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use(fileupload());

app.post('/resultaudio', async function(req, res) {
    //creation buffer file
    let file;

    //verification of file recovery
    if(!req.files)
    {
        res.send("File was not found");
        return;
    }
    
    //file recovery
    file = await req.files.file;  // here is the field name of the drag and drop
    console.log(file);

    fs.appendFile('./config/namefile.txt', file.name, function (err) {
        if (err) throw err;
    });
    
    //registration in path special
    await file.mv('./config/LA/ASVspoof2019_LA_eval/flac/' + file.name);
    //await file.mv('./' + file.name);
    
    //launch of the python analysis
    //creation of the one audio protocol by write in a txt file the name of the tested audio
    const child_Python1= await spawn('python', ['protocol.py']);
    child_Python1.stdout.on('data', async (data1)=>{
        scriptOutput1+= await data1;
        console.log(data1);
    });

    //launch the evaluation of the score of the file
    const child_Python2= await spawn('python', ['main.py']);
    child_Python2.stdout.on('data', async (data2)=>{
        console.log("hello");
        //ajout durée
        scriptOutput+= await data2;
        console.log(data2);

        //redirection to the result page
        res.redirect('/result');

        //deleting the buffer file
        const deleteFile = `./config/LA/ASVspoof2019_LA_eval/flac/${file.name}`;

        if (fs.existsSync(deleteFile)) {
            fs.unlink(deleteFile, (err) => {
                if (err) {
                    console.log(err);
                }
                console.log('deleted');
            })
        }  

    });
});

// redirect to home page
app.get('/', function(req, res) {
    res.render('pages/index'); 
});

// redirect to analyse page
app.get('/result', function(req, res) {

    // score display
    res.render('pages/result', {filescore:scriptOutput});

    // reset the score to 0
    const deleteFile2 = `./config/namefile.txt`;
    fs.readFile(deleteFile2, 'utf8', function(err, data)
    {
        if (err)
        {
            // check and handle err
            console.log('error');
        }
        var linesExceptFirst = data.split('\n').slice(1).join('\n');
        fs.writeFile(deleteFile2, linesExceptFirst, function(err, data) { if (err) {/** check and handle err */} });
    });
    scriptOutput="";
    scriptOutput1="";
});

// redirect to information page
app.get('/about', function(req, res) {
    res.render('pages/about');
});

// set the development port
app.listen(8080);
console.log('port: 8080');